<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Makanan extends Model
{
    protected  $table = "makanan";

    protected $fillable = [
        'judul',
        'content',
        'image'
    ];
}
