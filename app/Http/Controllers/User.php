<?php

namespace App\Http\Controllers;

use App\Models\Makanan;
use App\Models\Minuman;
use App\User as AppUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

class User extends Controller
{
    public function login()
    {
        return view('login');
    }

    public function index()
    {
        // return view('admin.dashboard');

        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Silahkan login');
        } else {
            $data_minuman = Minuman::all();
            $count_minuman = $data_minuman->count();

            $data_makanan = Makanan::all();
            $count_makanan = $data_makanan->count();
            return view(
                'dashboard',
                [
                    'count_makanan' => $count_makanan,
                    'count_minuman' => $count_minuman
                ]
            );
        }
    }

    public function loginPost(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $data = AppUser::where('email', $email)->first();
        if ($data) { //apakah email tersebut ada atau tidak
            if (Hash::check($password, $data->password)) {
                Session::put('user_id', $data->id);
                Session::put('name', $data->name);
                Session::put('email', $data->email);
                Session::put('login', TRUE);

                return redirect('dashboard');
            } else {
                return redirect('login')->with('alert', 'Password atau Email, Salah !');
            }
        } else {
            return redirect('login')->with('alert', 'Password atau Email, Salah!');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('login')->with('alert', 'Kamu sudah logout');
    }
}
