<?php

namespace App\Http\Controllers;

use App\Models\Makanan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class MakananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_makanan = Makanan::all();
            $count_makanan = $data_makanan->count();

            return view(
                'makanan.index',
                [
                    'data_makanan' => $data_makanan,
                    'count_makanan' => $count_makanan
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('makanan.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        if ($request->hasFile('image')) {
            $destination_path = 'public/images/products';
            $image = $request->file('image');

            $image_name = $image->getClientOriginalName();
            $extension = pathinfo($image_name)['extension'];
            $image_name = preg_replace('/\\.[^.\\s]{3,4}$/', '', $image_name);
            $image_name = Str::slug($image_name);
            $image_name = $image_name . '-' . Str::random(3) . '.' . $extension;
            // return ("$image_name.$extension");
            $path = $request->file('image')->storeAs($destination_path, $image_name);

            // $path = $request->file('image')->store($destination_path);
            // $image_name = $path->getClientOriginalName();

            $input['image'] = $image_name;
        }

        Makanan::create($input);
        return redirect('/makanan')->with('sukses', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_makanan = Makanan::find($id);

        return view(
            'makanan.edit',
            [
                'data_makanan' => $data_makanan,
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_makanan = Makanan::find($id);

        $data_makanan->update($request->all());

        return redirect('/makanan')->with('sukses', 'Data berhasil diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_makanan = Makanan::find($id);
        $data_makanan->delete();

        return redirect('/makanan')->with('sukses', 'Data berhasil dihapus');
    }

    // public function uploadForm()
    // {
    //     return view('makanan.create2');
    // }
    // public function uploadFile(Request $request)
    // {
    //     $path = $request->file->store('public');
    //     return "Success .$path.";
    // }
}
