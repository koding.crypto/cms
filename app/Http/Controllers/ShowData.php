<?php

namespace App\Http\Controllers;

use App\Models\Makanan;
use App\Models\Minuman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShowData extends Controller
{
    public function makanan()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_makanan = Makanan::all();
            $count_makanan = $data_makanan->count();

            return view(
                'showData.makanan',
                [
                    'data_makanan' => $data_makanan,
                    'count_makanan' => $count_makanan
                ]
            );
        }
    }

    public function minuman()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            $data_minuman = Minuman::all();
            $count_minuman = $data_minuman->count();

            return view(
                'showData.minuman',
                [
                    'data_minuman' => $data_minuman,
                    'count_minuman' => $count_minuman
                ]
            );
        }
    }
}
