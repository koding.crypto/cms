<?php

namespace App\Http\Controllers;

use App\Models\Makanan;
use App\Models\Minuman;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class Report extends Controller
{

    public function index()
    {
        if (!Session::get('login')) {
            return redirect('login')->with('alert', 'Kamu harus login dulu');
        } else {
            return view('report.index');
        }
    }

    public function download()
    {
        set_time_limit(300);
        $data_makanan = Makanan::all();
        $data_minuman = Minuman::all();


        $pdf = PDF::loadview('report.report-pdf', ['data_makanan' => $data_makanan, 'data_minuman' => $data_minuman])->setPaper('f4', 'landscape')->setWarnings(false);

        return $pdf->download('report.pdf');

        // return view('report.report-pdf', [
        //     'data_makanan' => $data_makanan,
        //     'data_minuman' => $data_minuman
        // ]);
        // return view('lap-transaksi.transaksi-pdf', ['data_komputer' => $data_komputer]);

    }
}
