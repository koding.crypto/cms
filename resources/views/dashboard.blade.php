@extends('layouts.master')
@section('title','CMS | Dashboard')
@section('additional-header')
@endsection
@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{$count_makanan}}</h3>

                        <p>Makanan</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-utensils"></i>
                    </div>
                    <a href="/list/makanan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-6 col-6">
                <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>{{$count_minuman}}</h3>

                        <p>Minuman</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-mug-hot"></i>
                    </div>
                    <a href="/list/minuman" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-12 connectedSortable">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="m-0">Makanan</h5>
                    </div>
                    <div class="card-body">
                        <img src="/adminlte/img/makanan-dan-minuman-khas-ramadhan.jpg" />

                        <p class="card-text">Makanan merupakan bahan, berasal dari tumbuhan atau hewan yang dikonsumsi oleh manusia dan makhluk hidup lainnya untuk bertahan hidup, menambah nutrisi dan energi sedangkan Minuman atau beverage mempunyai pengertian bahwa semua jenis cairan
                            yang dapat dimunum (drinkable liquid) kecuali obat-obatan. Minuman bagi
                            kehidupan manusia mempunyai beberapa fungsi yang mendasar yaitu: sebagai
                            penghilang rasa haus, perangsang nafsu makan, sebagai penambah tenaga, dan
                            sebagai sarana untuk membantu pencernaan makanan.</p>
                    </div>
                </div>

            </section>
            <!-- /.Left col -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection