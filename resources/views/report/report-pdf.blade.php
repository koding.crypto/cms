<!DOCTYPE html>
<html>

<head>
    <title>Report</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <style type="text/css">
        table tr td,
        table tr th {
            font-size: 9pt;
        }
    </style>
    <center>
        <h2>Data Makanan</h2>
    </center>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Judul</th>
                <th>Content</th>
                <th>Image</th>

            </tr>
        </thead>
        <tbody>
            @if($data_makanan ?? '' != '')
            @foreach($data_makanan as $makanan)
            <tr>
                <td>{{$makanan->id}}</td>
                <td>{{$makanan->judul}}</td>
                <td>{{$makanan->content}}</td>
                <!-- <td><img src="{{asset('/storage/images/products/'.$makanan->image)}}" /></td> -->
                <td><a href="{{asset('/storage/images/products/'.$makanan->image)}}">{{$makanan->image}}</a></td>
            </tr>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Judul</th>
                <th>Content</th>
                <th>Image</th>

            </tr>
        </tfoot>
    </table>

    <center>
        <h2>Data Minuman</h2>
    </center>
    <table id="example1" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Judul</th>
                <th>Content</th>
                <th>Image</th>

            </tr>
        </thead>
        <tbody>
            @if($data_minuman ?? '' != '')
            @foreach($data_minuman as $minuman)
            <tr>
                <td>{{$minuman->id}}</td>
                <td>{{$minuman->judul}}</td>
                <td>{{$minuman->content}}</td>
                <!-- <td><img src="{{asset('/storage/images/products/'.$minuman->image)}}" /></td> -->
                <td><a href="{{asset('/storage/images/products/'.$minuman->image)}}">{{$minuman->image}}</a></td>
            </tr>
            @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Judul</th>
                <th>Content</th>
                <th>Image</th>

            </tr>
        </tfoot>
    </table>
</body>

</html>