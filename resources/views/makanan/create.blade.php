@extends('layouts.master')
@section('title','CMS | Makanan')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Makanan</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Makanan</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Tambah Makanan</h3>
                    </div>

                    <form method="POST" enctype="multipart/form-data" action="{{route('makanan.store')}}">
                        @csrf
                        <div class="form-group">
                            <label>Judul:</label>
                            <input name="judul" type="text" class="form-control" id="judul" placeholder="Judul Makanan">
                        </div>
                        <div class="form-group">
                            <label>Content:</label>
                            <input name="content" type="text" class="form-control" id="content" placeholder="Content">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input name="image" type="file" class="custom-file-input" id="image">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="">Upload</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="submit">Upload</button>
                        </div>
                        <!-- <input type="file" name="image">
                        <button type="submit" name="submit">Upload</button> -->
                    </form>

                    <!-- /.card-header -->
                    <!-- form start -->
                    <!-- <form action="{{route('makanan.store')}}" method="POST" role="form" enctype="multipart/form-datas">
                        @csrf
                        <div class="card-body"> -->
                    <!-- <div class="form-group">
                                <label>Judul:</label>
                                <input name="judul" type="text" class="form-control" id="judul" placeholder="Judul Makanan">
                            </div>
                            <div class="form-group">
                                <label>Content:</label>
                                <input name="content" type="text" class="form-control" id="content" placeholder="Content">
                            </div> -->
                    <!-- <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input name="file" type="file" class="custom-file-input" id="file">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>
                                </div>
                            </div> -->
                    <!-- <input type="file" name="file">
                            <button type="submit" name="submit">Upload</button> -->
                </div>
                <!-- /.card-body -->

                <!-- <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form> -->
            </div>
            <!-- /.card -->
        </div>
        <!--/.col (left) -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->

@endsection

@section('additional')
<!-- jQuery -->
<script src="/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="/adminlte/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/adminlte/js/demo.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        bsCustomFileInput.init();
    });
</script>
</body>
@endsection