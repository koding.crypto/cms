<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return ('welcome');
});

Route::get('/login', 'User@login');
Route::get('/login', 'User@login');
Route::post('/loginPost', 'User@loginPost');
Route::get('/logout', 'User@logout');

Route::get('/dashboard', 'User@index');

Route::get('/report', 'Report@index');
Route::get('/report/download', 'Report@download');

Route::get('/list/makanan', 'ShowData@makanan');
Route::get('/list/minuman', 'ShowData@minuman');

Route::get('/upload', 'MakananController@uploadForm');
Route::post('/upload', 'MakananController@uploadFile');


Route::resource('/minuman', MinumanController::class);
Route::resource('/makanan', MakananController::class);

// Route::resource([
//     'makanan' => MakananController::class,
//     'minuman' => MinumanController::class,
// ]);
